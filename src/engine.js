const applyRule = require('./applyRule');
const {
    findStartRuleId,
    setCycleDetectionFlag
} = require('./utils');


function applyAllRules({ rules, target, onRuleSuccess, onRuleFail }) {
    let id = findStartRuleId(rules);
    const results = [];

    while (id != null) {
        const { isSuccess, nextId } = applyRule({
            rule: rules[id],
            target,
            onSuccess: onRuleSuccess,
            onFail: onRuleFail,
        });

        results.push({ isSuccess, id });
        id = nextId;
    }

    return results;
}

function engine({ ruleSet, target, onRuleSuccess, onRuleFail }) {
    const { id, rules } = ruleSet;
    console.log(`Applying rule set: ${id}`);

    if (rules.length === 0) {
        console.log('There are no rules to apply');
        console.log('End');
        return;
    }

    setCycleDetectionFlag(rules);
    return applyAllRules({ rules, target, onRuleSuccess, onRuleFail });
}

module.exports = engine;