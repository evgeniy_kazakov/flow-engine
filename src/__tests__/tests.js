const applyRule = require('../applyRule');
const engine = require('../engine');
const ruleSet = require('../../data/rules.json');

describe('Tests', () => {
    describe('applyRule', () => {
        let rule = null;
        beforeEach(() => {
            rule = {
                id: 'rule_1',
                body: 'function (target) { return target.x == 1; }',
                isCalled: false,
                successId: 'rule_2',
                failId: 'rule_3',
            };
        });

        it('should return success id', () => {
            const target = { x: 1 };
            expect(applyRule({ rule, target })).toEqual({ isSuccess: true, nextId: 'rule_2' });
        });

        it('should return fail id', () => {
            const target = { x: 2 };
            expect(applyRule({ rule, target })).toEqual({ isSuccess: false, nextId: 'rule_3' });
        });

        it('should throw error if body is invalid', () => {
            const target = { x: 2 };
            const invalidRule = Object.assign(rule, {
                body: 'function (obj)'
            });
            const msg = new RegExp(`Error in rule ${rule.id}. Body is invalid:`);

            expect(() => applyRule({ rule: invalidRule, target }))
                .toThrowError(msg);
        });

        it('should throw error if was called more then once', () => {
            const target = { x: 1 };

            applyRule({ rule, target });
            expect(() => applyRule({ rule, target }))
                .toThrowError(`Cycle detected. Rule ${rule.id} called more then once`)
            ;
        });

        it('should call success handler it if provided', () => {
            const target = { x: 1 };
            const onSuccess = jest.fn();
            applyRule({ rule, target, onSuccess });

            expect(onSuccess).toBeCalledWith(rule);
        });

        it('should call fail handler it if provided', () => {
            const target = { x: 2 };
            const onFail = jest.fn();
            applyRule({ rule, target, onFail });

            expect(onFail).toBeCalledWith(rule);
        });
    });

    describe('engine', () => {
        it('should return list of applied ruled', () => {
            const target = {
                x: 2,
                y: 4,
                g: 'hello'
            };
            const expectedResults = [
                { isSuccess: true, id: 'rule_1' },
                { isSuccess: true, id: 'rule_2' },
                { isSuccess: true, id: 'rule_4' }
            ];

            expect(engine({ ruleSet, target })).toEqual(expectedResults);
        });
    })
});