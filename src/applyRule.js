const {
    tryToInvoke,
    noop,
} = require('./utils');

function applyRule({ rule, target, onSuccess = noop, onFail = noop}) {
    const {
        id,
        body,
        successId,
        failId
    } = rule;

    // It's possible for rule to be called more then once without being infinite loop
    // but this is trade off for reliability
    if (rule.isCalled) {
        throw Error(`Cycle detected. Rule ${id} called more then once`);
    }

    rule.isCalled = true;

    const fn = tryToInvoke(
        () => Function('params', `
            return (${body})(params);
        `),
        (error) => {
            throw new Error(`Error in rule ${id}. Body is invalid: ${error.message}`);
        }
    );

    const isSuccess = fn(target);



    if (isSuccess) {
        onSuccess(rule);
    } else {
        onFail(rule);
    }

    return {
        isSuccess,
        nextId: isSuccess ? successId : failId,
    };
}

module.exports = applyRule;