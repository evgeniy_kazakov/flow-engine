const engine = require('./engine');

if (require.main === module) {
    const [
        ruleSetPath,
        targetPath,
    ] = process.argv.slice(2);
    console.log(`Rules file: ${ruleSetPath}`);
    console.log(`Target file: ${targetPath}`);

    const fs = require('fs');
    const path = require('path');

    const { tryToInvoke } = require('./utils');
    const rawRuleSet = fs.readFileSync(path.resolve(__dirname, ruleSetPath), 'utf8');
    const rawTarget = fs.readFileSync(path.resolve(__dirname, targetPath), 'utf8');
    const ruleSet = tryToInvoke(
        () => JSON.parse(rawRuleSet),
        (error) => console.log(`Failed to parse rules json: ${error.message}`)
    );
    const target = tryToInvoke(
        () => JSON.parse(rawTarget),
        (error) => console.log(`Failed to parse target json: ${error.message}`)
    );
    const onRuleSuccess = ({ id }) => console.log('\x1b[32m%s\x1b[0m', `Success ${id}`);
    const onRuleFail = ({ id }) => console.log('\x1b[31m%s\x1b[0m', `Fail ${id}`);

    engine({
        ruleSet,
        target,
        onRuleSuccess,
        onRuleFail
    });
} else {
    module.exports = engine;
}