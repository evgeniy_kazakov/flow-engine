function findStartRuleId(rules) {
    const rule = Object
        .values(rules)
        .find((rule) => rule.isFirst)
    ;

    if (rule) {
        return rule.id;
    }
}

function setCycleDetectionFlag(rules) {
    Object.values(rules).forEach((rule) => {
        rule.isCalled = false;
    });
}

function tryToInvoke(fn, errorCallback) {
    try {
        return fn();
    } catch (error) {
        errorCallback(error);
    }
}

const noop = () => {};

module.exports = {
    noop,
    tryToInvoke,
    findStartRuleId,
    setCycleDetectionFlag,
};